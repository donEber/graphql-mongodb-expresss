module.exports = `type Video{
    id: ID!
    title: String
    views: Int
}
input VideoInput{
    title: String!
    views: Int
}
extend type Query{
    getVideos: [Video]
    getVideo(id: ID!): Video
}
extend type Mutation{
    addVideo( input: VideoInput ):Video
    updateVideo(id: ID!, input: VideoInput ): Video
    deleteVideo(id: ID!): String
}`
