const Video = require('./../models/video')
module.exports = {
    Query: {
        async getVideos(){
            return await Video.find()
        },
        getVideo(obj,{ id }) {
            return videos.find(v => v.id == id)
        },
    },
    Mutation: {
        async addVideo(obj,{ input }) {
            const newVideo = new Video(input)
            await newVideo.save()
            return newVideo
        },
        async updateVideo(obj,{ id, input }) {
            return await Video.findByIdAndUpdate(id,input)
        },
        async deleteVideo(obj,{ id }) {
            await Video.findByIdAndDelete(id)
            return `Se elimino el video con id ${id}`
        }
    }
}
