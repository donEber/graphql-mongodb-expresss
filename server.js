const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const { ApolloServer } = require('apollo-server-express')


mongoose.connect('mongodb://localhost/graphql_db')

const app = express()

const videoTyepesDefs = require('./types/video.types')
const resolvers = require('./resolvers/video.resolvers')
const typeDefs = `
    type Alert{
        message: String!
    }
    type Query{
        _ : Boolean
    }
    type Mutation{
        _ : Boolean
    }
`

const server = new ApolloServer({
    typeDefs:[typeDefs,videoTyepesDefs],
    resolvers
})

server.applyMiddleware({app})
app.listen(8081, ()=>{
    console.log("Servidor en el puerto 8081")
})