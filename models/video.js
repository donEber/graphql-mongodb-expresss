const mongoose = require('mongoose')

const videoSchema = new mongoose.Schema({
    title: String,
    views: Number
})

module.exports = mongoose.model('Video', videoSchema)